<?php include("template/header.php");?>
    <?php include("template/navigation.php");?>
	    <div id="conteudo">
		    <div class="container-fluid">
		      <div class="row">
				<div class="col-sm-8 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		          <h1 class="page-header">Lista Telefonica</h1>
		
		          <div class="table-responsive">
		            <table class="table table-striped">
		              <thead>
		                <tr>
		                  <th>#</th>
		                  <th>Nome</th>
		                  <th>Sobrenome</th>
		                  <th>Telefone</th>
		                  <th>Ações</th>
		                </tr>
		              </thead>
		              <tbody>
		              <?php
                    	$consulta = mysql_query("SELECT * FROM agenda"); 
                    	while($campo = mysql_fetch_array($consulta)){
                		?>
		                <tr>
		                  <td><?php echo $campo['id'];?></td>
		                  <td><?php echo $campo['nome'];?></td>
		                  <td><?php echo $campo['sobrenome'];?></td>
		                  <td><?php echo $campo['telefone'];?></td>
		                  <td>
							<a href="<?=URL_SITE?>/cadastrar?id=<?php echo $campo['id'];?>" title="editar" >
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</a>
							&nbsp;
							<a href="#" title="excluir" onclick="javascript: if (confirm('Você realmente deseja excluir este registro?'))location.href='excluir.php?id=<?php echo $campo['id'];?>'">
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
							</a>
						  </td>
		                </tr>
		                <?php } ?>
		              </tbody>
		            </table>
		          </div>
		        </div>
				<div class="col-sm-2 col-md-2">
		          
		        </div>
				
		      </div>
		    </div>
	    	
	    </div>
<?php include ("template/footer.php")?>