function vaiPara(strPag) {
	location.replace(strPag);
	return false;
}

function confirmaPara(strPag, strMens) {
	if (confirm(strMens))
		location.replace(strPag);
	return false;
}

function Mascara(o, f) {
	v_obj = o;
	v_fun = f;
	setTimeout("execmascara()", 1);
}

function execmascara() {
	v_obj.value = v_fun(v_obj.value);
}

function soNums(v) {
	return v.replace(/\D/g, "");
}

function Telefone(v) {
	v = v.replace(/\D/g, "");
	v = v.replace(/^(\d\d)(\d)/g, "($1) $2");
	v = v.replace(/(\d{4})(\d)/, "$1-$2");
	return v;
}

function TelefoneCall(v) {
	v = v.replace(/\D/g, "");
	v = v.replace(/^(\d\d)(\d)/g, "($1) $2");
	return v;
}