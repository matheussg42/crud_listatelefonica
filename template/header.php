<?php
    require_once 'config/conexao.class.php';
    require_once 'config/crud.class.php';

    $con = new conexao();
    $con->connect();
    
    
    if(isset($_SERVER["SERVER_PORT"]))
    	define("URL_SITE", "http://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]."/ListaTelefonica");
    else
    	define("URL_SITE", "http://".$_SERVER["SERVER_NAME"]."/ListaTelefonica");
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Agenda telefonica">
    <meta name="author" content="Matheus">

    <title>Agenda Telefonica</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    
  </head>