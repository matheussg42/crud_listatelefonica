<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
          	<img class="logo" alt="Print4me" src="img/logo.png" onmouseover="this.src='img/logo-hover.png'" onmouseout="this.src='img/logo.png'" />
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a href="<?=URL_SITE?>/home"> Agenda</a></li>
            <li><a href="<?=URL_SITE?>/cadastrar"> Cadastrar Novo</a></li>
            
          </ul>
        </div>
      </div>
    </nav>